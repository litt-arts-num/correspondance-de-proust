# Correspondance de Proust

Version complètement réécrite, et les différentes dépendances mises à jour
- Symfony 5
- Saxon
- PHP 7

## Installation
- copier le .env.dist en .env
- copier le ./application/.env.dist en ./application/.env
- `docker-compose build`
- `docker-compose up -d`
- `docker-compose exec apache bash`
  - `make update`
  - créer un compte puis :
  - `php bin/console app:set-role <mail> ROLE_SUPER_ADMIN`

## Mise à jour
- git pull
- `docker-compose exec apache bash`
  - `make update`
  - ou bien `npm run dev` peut suffire


## Commandes

* passer un compte en admin :
`php bin/console app:set-role <mail> ROLE_SUPER_ADMIN`

* supprimer toutes les lettres
`php bin/console app:delete-letters`

* import letters
copy xml files into `application/import` && `php bin/console app:import`

* supprimer le cache pour une lettre
`php bin/console app:cache:letter:invalidate <letterId>`

* supprimer le cache pour toutes les lettres
`php bin/console app:cache:invalidate `

* Préparer le cache pour toutes les lettres
`php bin/console app:cache:warmup `

## Autre
on passe d'une instance de prod à préprod en changeant la valeur d'`INSTANCE` dans `application/.env`. Ajouter la ligne si besoin, en prenant exemple sur  `application/.env.dist` (dans le cas ou l'installation de l'instance est antérieur au développement de cette fonctionnalité)
