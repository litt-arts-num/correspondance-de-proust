import '../styles/letters.scss';
import 'tom-select/dist/css/tom-select.bootstrap5.min.css';
import TomSelect from 'tom-select/dist/js/tom-select.complete.min.js';
import noUiSlider from 'nouislider';
import 'nouislider/dist/nouislider.min.css';
import wNumb from 'wnumb'
import { createApp } from 'vue'

createApp({
  compilerOptions: {
    delimiters: ["${", "}"]
  },
  data() {
    return {
      toto: 0,
      tomselect: {
        options: {
          allowEmptyOption: true,
          plugins: ['clear_button'],
          create: false,
          onFocus: function() {
            this.clearOptions()
            this.sync()
          }
        },
        from: null,
        to: null,
        id: null,
        tags: null
      },
      refreshKey: false,
      slider: null,
      filters: {
        lowerBoundary: -2051222961000,
        higherBoundary: -1546300800000,
        from: '',
        to: '',
        id: '',
        tags: '',
        published: 'published'
      },
      letters: [],
      publishedIds: [],
      admin: 0
    }
  },

  mounted() {
    this.letters = JSON.parse(document.getElementById('app').dataset.json).sort(this.compare);
    this.publishedIds = JSON.parse(document.getElementById('app').dataset.publishedids)
    this.admin = document.getElementById('app').dataset.admin;
    this.filters.published = this.admin && document.getElementById('app').dataset.instance == "preprod" ? 'all' : 'published'
    this.tomselect.from = new TomSelect("#expediteurs", this.tomselect.options);
    this.tomselect.to = new TomSelect("#destinataires", this.tomselect.options);
    this.tomselect.id = new TomSelect("#corr-proust-id", this.tomselect.options);
    this.tomselect.tags = new TomSelect("#tags", this.tomselect.options);

    var range = document.getElementById('range');
    this.slider = noUiSlider.create(range, {
      step: 24 * 60 * 60 * 1000,
      start: [this.getMinValue(), this.getMaxValue()],
      range: {
        'min': this.getMinValue(),
        'max': this.getMaxValue()
      },
      format: wNumb({
        decimals: 0
      })
    });

    var dateValues = [
      document.getElementById('event-start'),
      document.getElementById('event-end')
    ];
    var formatter = new Intl.DateTimeFormat();

    this.slider.on('update', function(values, handle) {
      dateValues[handle].innerHTML = formatter.format(new Date(+values[handle]));
      let lower = document.getElementById('lower')
      lower.value = values[0]
      lower.dispatchEvent(new Event('input', {
        bubbles: true
      }));
      let higher = document.getElementById('higher')
      higher.value = values[1]
      higher.dispatchEvent(new Event('input', {
        bubbles: true
      }));
    });
  },

  methods: {
    compare(a, b) {
      if (a.boundaries.lower < b.boundaries.lower) {
        return -1;
      }
      if (a.boundaries.lower > b.boundaries.lower) {
        return 1;
      }
      // si égalité temporelle, on compare les id.
      if(a.id < b.id) {
        return -1;
      } 
      
      return 1;
    },

    getMinValue() {
      return Math.min(...this.letters.map(letter => letter.boundaries.lower))
    },
    getMaxValue() {
      return Math.max(...this.letters.map(letter => letter.boundaries.higher))
    },
    getDistinctValues(property) {
      return [...new Set(
        this.filteredLetter().map(item => item[property])
      )].sort()
    },
    getDistinctArrayValues(property) {
      return [...new Set(
        this.filteredLetter().map(item => item[property]).flat()
      )].sort(function(a, b) {
        return a.localeCompare(b);
      })
    },
    generateUrl(id) {
      return Routing.generate('letter', {
        'proustId': id
      });
    },
    filteredLetter() {
      return this.letters.filter(letter => {
        return (this.publishedIds.includes(letter.id) || this.admin == '1') &&
          (this.filters.from == "" || letter.from_canonical.includes(this.filters.from)) &&
          (this.filters.to == "" || letter.to_canonical.includes(this.filters.to)) &&
          (this.filters.id == "" || letter.id.includes(this.filters.id)) &&
          (this.filters.tags == "" || letter.tags.includes(this.filters.tags)) &&
          (this.filters.lowerBoundary <= letter.boundaries.lower && this.filters.higherBoundary >= letter.boundaries.higher) &&
          (
            this.filters.published == "published" && this.publishedIds.includes(letter.id) ||
            this.filters.published == "unpublished" && this.publishedIds.includes(letter.id) == false ||
            this.filters.published == 'all'
          )
      });
    },
    timestamp(str) {
      return new Date(str).getTime();
    }
  }

}).mount('#app')
