import {
  Popover
} from 'bootstrap';

window.addEventListener('load', (event) => {
  let personSpans = document.querySelectorAll('.person-popover')
  personSpans.forEach(function(personSpan) {
    personSpan.addEventListener("mouseover", function(event) {
      let span = event.target.closest('.person-popover')
      let key = span.dataset.key
      if (key && span.dataset.bsContent == "...") {
        let url = Routing.generate('info-person', {
          'key': key
        });

        fetch(url)
          .then((response) => {
            if (!response.ok) {
              throw new Error('Network response was not OK');
            }
            return response.text();
          })
          .then((data) => {
            let popover = Popover.getInstance(span);
            popover._config.content = data;
            popover.setContent();
            span.setAttribute('data-bs-content', '')

            // set content for all spans about the same person.
            let samePersonSpans = document.querySelectorAll('.person-popover[data-key='+key+']')
            samePersonSpans.forEach(function(samePersonSpan) {
              let popover = Popover.getInstance(samePersonSpan);
              popover._config.content = data;
              popover.setContent();
              span.setAttribute('data-bs-content', '')
            })

          })
          .catch((error) => {
            console.error('There has been a problem with your fetch operation:', error);
          });
      }

    }, true);
  });
});
