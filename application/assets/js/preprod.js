import DataTable from 'datatables.net-bs5';
import 'datatables.net-bs5/css/dataTables.bootstrap5.min.css';

document.addEventListener('DOMContentLoaded', function () {
    let table = new DataTable('#table', {
      paging: false
    });
});
