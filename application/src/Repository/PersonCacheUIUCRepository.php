<?php

namespace App\Repository;

use App\Entity\PersonCacheUIUC;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @extends ServiceEntityRepository<PersonCacheUIUC>
 *
 * @method PersonCacheUIUC|null find($id, $lockMode = null, $lockVersion = null)
 * @method PersonCacheUIUC|null findOneBy(array $criteria, array $orderBy = null)
 * @method PersonCacheUIUC[]    findAll()
 * @method PersonCacheUIUC[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class PersonCacheUIUCRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, PersonCacheUIUC::class);
    }

    public function add(PersonCacheUIUC $entity, bool $flush = true): void
    {
        $this->getEntityManager()->persist($entity);

        if ($flush) {
            $this->getEntityManager()->flush();
        }
    }

    public function create($key, $content)
    {
        $cache = new PersonCacheUIUC();
        $cache->setName($key);
        $cache->setContent($content);

        $this->add($cache);
    }

    public function remove(PersonCacheUIUC $entity, bool $flush = false): void
    {
        $this->getEntityManager()->remove($entity);

        if ($flush) {
            $this->getEntityManager()->flush();
        }
    }

//    /**
//     * @return PersonCacheUIUC[] Returns an array of PersonCacheUIUC objects
//     */
//    public function findByExampleField($value): array
//    {
//        return $this->createQueryBuilder('p')
//            ->andWhere('p.exampleField = :val')
//            ->setParameter('val', $value)
//            ->orderBy('p.id', 'ASC')
//            ->setMaxResults(10)
//            ->getQuery()
//            ->getResult()
//        ;
//    }

//    public function findOneBySomeField($value): ?PersonCacheUIUC
//    {
//        return $this->createQueryBuilder('p')
//            ->andWhere('p.exampleField = :val')
//            ->setParameter('val', $value)
//            ->getQuery()
//            ->getOneOrNullResult()
//        ;
//    }
}
