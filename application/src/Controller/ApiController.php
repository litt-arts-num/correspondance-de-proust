<?php

namespace App\Controller;

use App\Entity\Letter;
use App\Service\LetterManager;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\Routing\Annotation\Route;

class ApiController extends AbstractController
{
    /**
     * @Route("/api/letters-preprod", name="api_preprod_letters")
     */
    public function apiLetters(EntityManagerInterface $em, LetterManager $lm): JsonResponse
    {
        $xmlPath = $this->getParameter('letters_path') . DIRECTORY_SEPARATOR . "test.xml";
        $xslPath = $this->getParameter('xslt_path') . DIRECTORY_SEPARATOR . "corr-proust_tei2html_askme.xsl";
        $json = $lm->lettersToJson($xmlPath, $xslPath);

        return JsonResponse::fromJsonString($json);
    }

    /**
     * @Route("/api/letter-preprod/{proustId}", name="api_preprod_letter")
     */
    public function apiLetter(Letter $letter): JsonResponse
    {
        $xmlPath = $this->getParameter('letters_path') . DIRECTORY_SEPARATOR . $letter->getFilename();
        $xmlContent = file_get_contents($xmlPath);

        $array = ["content" => $xmlContent, "filename" => $letter->getFilename()];

        return new JsonResponse($array);
    }
}
