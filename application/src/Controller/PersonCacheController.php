<?php

namespace App\Controller;

use App\Entity\PersonCacheUIUC;
use App\Service\PersonCacheManager;
use Doctrine\ORM\EntityManagerInterface;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
     * @Route("/cache/", name="person_cache_")
     * @IsGranted("ROLE_ADMIN")
     */
class PersonCacheController extends AbstractController
{
    /**
     * @Route("list", name="list")
     */
    public function list(EntityManagerInterface $em): Response
    {
        $cachePersons = $em->getRepository(PersonCacheUIUC::class)->findBy([],["name" => "ASC"]);

        return $this->render('person/list.html.twig', [
            'cachePersons' => $cachePersons
        ]);
    }

    /**
     * @Route("update/{id}", name="update")
     */
    public function update(PersonCacheUIUC $person, PersonCacheManager $pcm): Response
    {
        $pcm->update($person);

        return $this->redirectToRoute("person_cache_list");
    }
}
