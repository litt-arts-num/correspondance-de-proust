<?php

namespace App\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Validator\Constraints\NotBlank;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;


class TextSearchType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        $builder
            ->add('query', TextType::class, [
              'label' => false,
              'required' => true,
              'attr' => [
                'placeholder' => "type_query_here"
              ],
              'constraints' => [
                  new NotBlank([
                      'message' => 'Please enter a query',
                  ]),
              ],
            ])

            ->add('search_in', ChoiceType::class, [
                'label' => false,
                'required' => true,
                'choices' => [
                    'search_in_text' => 'text',
                    'search_in_metadata' => 'metadata',
                    'search_in_notes' => 'notes'
                ],
                'data' => ['text'],
                'expanded' => true,
                'multiple' => true
            ])

            ->add('case_sensitive', CheckboxType::class, [
                'label' => 'case_sensitive',
                'required' => false,
                'attr' => [
                    'checked' => false
                ]
            ])

            ->add('save', SubmitType::class, [
                'attr' => ['class' => 'btn btn-secondary'],
                'label' => 'search',
            ]);;
    }

    public function configureOptions(OptionsResolver $resolver): void
    {
        $resolver->setDefaults([
        ]);
    }
}
