<?php

namespace App\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\FileType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Validator\Constraints\File;

class XmlUploadType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        $builder
        ->add('xml', FileType::class, [
          'label' => 'xml_file',
          'required' => true,
          'mapped' => false,
          'constraints' => [
            new File([
              'mimeTypes' => [ // We want to let upload only txt, csv or Excel files
                'text/xml',
                'application/xml'
              ],
              'mimeTypesMessage' => "This document isn't valid.",
            ])
          ],
        ])
        ->add('save', SubmitType::class, [
          'attr' => ['class' => 'btn btn-primary'],
          'label' => 'submit',
        ]);
    }

    public function configureOptions(OptionsResolver $resolver): void
    {
        $resolver->setDefaults([
            // Configure your form options here
        ]);
    }
}
