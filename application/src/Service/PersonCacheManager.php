<?php

namespace App\Service;

use App\Entity\Person;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Contracts\HttpClient\HttpClientInterface;


class PersonCacheManager
{
    private $em;
    private $client;
    private $uiucBaseUrl; 

    public function __construct(EntityManagerInterface $em, HttpClientInterface $client)
    {
        $this->em = $em;
        $this->client = $client;
        $this->uiucBaseUrl = "https://quest.library.illinois.edu/KP/";
    }

    public function update($person)
    {
        $key = $person->getName();
        $content = $this->getInfo($key);

        $person->setContent($content);
        $this->em->persist($person);
        $this->em->flush();

        return;
    }

    public function getInfo($key)
    {
        $response = $this->client->request("GET", $this->uiucBaseUrl . $key . ".jsonld");
        $content = $response->getContent();

        return $content;
    }
}
