<?php

namespace App\Service;

use Doctrine\ORM\EntityManagerInterface;
use Saxon\SaxonProcessor;
use Symfony\Component\DomCrawler\Crawler;
use Symfony\Component\HttpFoundation\Session\Flash\FlashBagInterface;
use Symfony\Component\Filesystem\Filesystem;
use Symfony\Component\Finder\Finder;
use App\Entity\Letter;

class XMLManager
{
    private $flash;
    private $em;

    public function __construct(FlashBagInterface $flash, EntityManagerInterface $em)
    {
        $this->flash = $flash;
        $this->em = $em;
    }

    public function initCrawler($path)
    {
        $xml = file_get_contents($path);
        $crawler = new Crawler($xml);

        return $crawler;
    }

    public function extract(Crawler $crawler, $tag, $attribute = null)
    {
        $data = $crawler->filter($tag)->first();
        if ($data->count() > 0) {
            if ($attribute) {
                if ($data->attr($attribute)) {
                    return $data->attr($attribute);
                }
            } else {
                return $data->text();
            }
        }

        $strError = "Problème avec l'élement / attribut : " . $tag;
        $strError .= ($attribute) ? " @" . $attribute : "";
        $this->flash->add('danger', $strError);

        return null;
    }

    public function extractAll(Crawler $crawler, $tag, $attribute = null)
    {
        $data = $crawler->filter($tag)->extract(['_text']);
        if ($attribute) {
            $data = $data->attr($attribute);
        }

        return $data;
    }

    public function testSaxon($xmlPath, $xslPath, $what)
    {
        $letter = basename($xmlPath);
        $cacheDir = "/var/www/public/cache/";

        $filesystem = new Filesystem();
        $path = $cacheDir . 'letter-' . $letter . '-what-' . $what . '.html';
        if ($filesystem->exists($path)) {
            $html = file_get_contents($path);
            return $html;
        }

        $saxonProc = new SaxonProcessor();

        $xsltProc = $saxonProc->newXsltProcessor();
        $xsltProc->setSourceFromFile($xmlPath);
        $xsltProc->compileFromFile($xslPath);

        $xsltProc->setParameter('query', $saxonProc->createAtomicValue($what));
        $xsltProc->setParameter('xml-dir', $saxonProc->createAtomicValue("/var/www/public/upload/letters/"));
        $xml = $xsltProc->transformToString();

        $xsltProc->clearParameters();
        $xsltProc->clearProperties();
        unset($xsltProc);
        unset($saxonProc);

        $filesystem->dumpFile($path, $xml);

        return $xml;
    }

    public function testOneLetter($xmlPath, $xslPath)
    {
        $saxonProc = new SaxonProcessor();

        $xsltProc = $saxonProc->newXsltProcessor();
        $xsltProc->setSourceFromFile($xmlPath);
        $xsltProc->compileFromFile($xslPath);

        $xsltProc->setParameter('query', $saxonProc->createAtomicValue("json_one"));
        $xsltProc->setParameter('xml-dir', $saxonProc->createAtomicValue("/var/www/public/upload/letters/"));
        $xml = $xsltProc->transformToString();

        if ($xml == null) {
            $errCount = $xsltProc->getExceptionCount();
            if ($errCount > 0) {
                for ($i = 0; $i < $errCount; $i++) {
                    $errCode = $xsltProc->getErrorCode(intval($i));
                    $errMessage = $xsltProc->getErrorMessage(intval($i));
                    $msg  = 'Error: Code=' . $errCode . " // ";
                    $msg .= 'Message=' . $errMessage;
                }
                $xsltProc->exceptionClear();

                return $msg;
            }
        }

        $xsltProc->clearParameters();
        $xsltProc->clearProperties();
        unset($xsltProc);
        unset($saxonProc);

        return true;
    }

    public function getEditorial($xslPath, $xmlPath, $slug, $lang)
    {
        $saxonProc = new SaxonProcessor();

        $xsltProc = $saxonProc->newXsltProcessor();
        $xsltProc->setSourceFromFile($xmlPath);
        $xsltProc->compileFromFile($xslPath);

        $xsltProc->setParameter('slug', $saxonProc->createAtomicValue($slug));
        $xsltProc->setParameter('lang', $saxonProc->createAtomicValue($lang));

        $xml = $xsltProc->transformToString();

        $xsltProc->clearParameters();
        $xsltProc->clearProperties();
        unset($xsltProc);
        unset($saxonProc);

        return $xml;
    }
    public function cacheInvalidate()
    {
        $cacheDir = "/var/www/public/cache/";
        $filesystem = new Filesystem();
        $finder = new Finder;
        $finder->in($cacheDir)->files()->name('*.html');

        foreach ($finder as $file) {
            $filesystem->remove($file->getRealPath());
        }

        return;
    }

    public function letterCacheInvalidate(Letter $letter)
    {
        $cacheDir = "/var/www/public/cache/";
        $filesystem = new Filesystem();
        $finder = new Finder;
        
        $filename = $letter->getFilename();
        $finder->in($cacheDir)->files()->name('letter-'.$filename."*.html");
        foreach ($finder as $file) {
            $filesystem->remove($file->getRealPath());
        }

        $filename = "test.xml";
        $finder->in($cacheDir)->files()->name('letter-'.$filename."*.html");
        foreach ($finder as $file) {
            $filesystem->remove($file->getRealPath());
        }

        return;
    }

    public function cacheWarmup()
    {
        $letters = $this->em->getRepository(Letter::class)->findAll();
        $xslPath = "/var/www/public/data/xslt/corr-proust_tei2html_askme.xsl";

        foreach ($letters as $letter) {
            echo "\n --------- ". $letter->getProustId(). "\n";
            $xmlPath = "/var/www/public/upload/letters/" . $letter->getFilename();
            $this->testSaxon($xmlPath, $xslPath, "title");
            $this->testSaxon($xmlPath, $xslPath, "diplo");
            $this->testSaxon($xmlPath, $xslPath, "linear");
            $this->testSaxon($xmlPath, $xslPath, "note");
            $this->testSaxon($xmlPath, $xslPath, "metadata");
            $this->testSaxon($xmlPath, $xslPath, "facs");
            $this->testSaxon($xmlPath, $xslPath, "debug-info");
            echo "\n ---------\n";  
        }

        return;
    }
}
