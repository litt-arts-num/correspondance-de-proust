<?php

namespace App\Service;

use App\Service\UserManager;
use Symfony\Component\HttpFoundation\Request;

class LocaleManager
{
    private $um;

    public function __construct(UserManager $um)
    {
        $this->um = $um;
    }

    /**
     * This methond returns the user locale and store it in session, if there is no user this method return default
     * language or the browser language if it is present in translations.
     *
     * @return string the locale string as en, fr, es, etc
     */
    public function getUserLocale(Request $request)
    {
        switch (true) {
            case ($user = $this->um->getCurrentUser()) && ($locale = $user->getLocale()):
                break;
            case $locale = $request->getSession()->get('_locale'):
                break;
            case $locale = $request->attributes->get('_locale'):
                break;
        }

        if (!$locale) {
            $locale = 'fr';
        }
        $request->getSession()->set('_locale', $locale);

        return $locale;
    }
}
