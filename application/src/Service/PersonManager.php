<?php

namespace App\Service;

use App\Entity\Person;
use Doctrine\ORM\EntityManagerInterface;

class PersonManager
{
    private $em;

    public function __construct(EntityManagerInterface $em)
    {
        $this->em = $em;
    }

    public function getOrCreate($name)
    {
        if (!$person = $this->em->getRepository(Person::class)->findOneByName($name)) {
            $person = new Person();
            $person->setName($name);

            $this->em->persist($person);
        }
        return $person;
    }
}
