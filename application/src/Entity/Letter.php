<?php

namespace App\Entity;

use App\Repository\LetterRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=LetterRepository::class)
 */
class Letter
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="boolean")
     */
    private $published;

    /**
     * @ORM\Column(type="string", length=20)
     */
    private $proustId;

    /**
     * @ORM\Column(type="datetime")
     */
    private $creationDate;

    /**
     * @ORM\Column(type="datetime")
     */
    private $updateDate;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $filename;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $originalFilename;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $humanDate;

    /**
     * @ORM\ManyToOne(targetEntity=Person::class, inversedBy="sentLetters")
     * @ORM\JoinColumn(nullable=true)
     */
    private $sender;

    /**
     * @ORM\ManyToOne(targetEntity=Person::class, inversedBy="receivedLetters")
     * @ORM\JoinColumn(nullable=true)
     */
    private $recipient;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getPublished(): ?bool
    {
        return $this->published;
    }

    public function setPublished(bool $published): self
    {
        $this->published = $published;

        return $this;
    }

    public function getProustId(): ?string
    {
        return $this->proustId;
    }

    public function setProustId(string $proustId): self
    {
        $this->proustId = $proustId;

        return $this;
    }

    public function getCreationDate(): ?\DateTimeInterface
    {
        return $this->creationDate;
    }

    public function setCreationDate(\DateTimeInterface $creationDate): self
    {
        $this->creationDate = $creationDate;

        return $this;
    }

    public function getUpdateDate(): ?\DateTimeInterface
    {
        return $this->updateDate;
    }

    public function setUpdateDate(\DateTimeInterface $updateDate): self
    {
        $this->updateDate = $updateDate;

        return $this;
    }

    public function getFilename(): ?string
    {
        return $this->filename;
    }

    public function setFilename(string $filename): self
    {
        $this->filename = $filename;

        return $this;
    }

    public function getOriginalFilename(): ?string
    {
        return $this->originalFilename;
    }

    public function setOriginalFilename(string $originalFilename): self
    {
        $this->originalFilename = $originalFilename;

        return $this;
    }

    public function getHumanDate(): ?string
    {
        return $this->humanDate;
    }

    public function setHumanDate(?string $humanDate): self
    {
        $this->humanDate = $humanDate;

        return $this;
    }

    public function getSender(): ?Person
    {
        return $this->sender;
    }

    public function setSender(?Person $sender): self
    {
        $this->sender = $sender;

        return $this;
    }

    public function getRecipient(): ?Person
    {
        return $this->recipient;
    }

    public function setRecipient(?Person $recipient): self
    {
        $this->recipient = $recipient;

        return $this;
    }
}
