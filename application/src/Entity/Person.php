<?php

namespace App\Entity;

use App\Repository\PersonRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=PersonRepository::class)
 */
class Person
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $name;

    /**
     * @ORM\OneToMany(targetEntity=Letter::class, mappedBy="sender")
     */
    private $sentLetters;

    /**
     * @ORM\OneToMany(targetEntity=Letter::class, mappedBy="recipient")
     */
    private $receivedLetters;

    public function __construct()
    {
        $this->sentLetters = new ArrayCollection();
        $this->receivedLetters = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(string $name): self
    {
        $this->name = $name;

        return $this;
    }

    /**
     * @return Collection<int, Letter>
     */
    public function getSentLetters(): Collection
    {
        return $this->sentLetters;
    }

    public function addSentLetter(Letter $sentLetter): self
    {
        if (!$this->sentLetters->contains($sentLetter)) {
            $this->sentLetters[] = $sentLetter;
            $sentLetter->setSender($this);
        }

        return $this;
    }

    public function removeSentLetter(Letter $sentLetter): self
    {
        if ($this->sentLetters->removeElement($sentLetter)) {
            // set the owning side to null (unless already changed)
            if ($sentLetter->getSender() === $this) {
                $sentLetter->setSender(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection<int, Letter>
     */
    public function getReceivedLetters(): Collection
    {
        return $this->receivedLetters;
    }

    public function addReceivedLetter(Letter $receivedLetter): self
    {
        if (!$this->receivedLetters->contains($receivedLetter)) {
            $this->receivedLetters[] = $receivedLetter;
            $receivedLetter->setRecipient($this);
        }

        return $this;
    }

    public function removeReceivedLetter(Letter $receivedLetter): self
    {
        if ($this->receivedLetters->removeElement($receivedLetter)) {
            // set the owning side to null (unless already changed)
            if ($receivedLetter->getRecipient() === $this) {
                $receivedLetter->setRecipient(null);
            }
        }

        return $this;
    }
}
