<?php

namespace App\Command;

use App\Entity\User;
use App\Service\UserManager;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Style\SymfonyStyle;

class SetRoleCommand extends Command
{
    protected static $defaultName = 'app:set-role';
    protected static $defaultDescription = 'Set role for a given user.';
    private $em;
    private $um;

    public function __construct(EntityManagerInterface $em, UserManager $um)
    {
      $this->um = $um;
      $this->em = $em;
      parent::__construct();
    }

    protected function configure(): void
    {
      $this
          ->setDescription('Switch status for a given user')
          ->addArgument('mail', InputArgument::REQUIRED, 'mail')
          ->addArgument('role', InputArgument::REQUIRED, 'role')
      ;
    }

    protected function execute(InputInterface $input, OutputInterface $output): int
    {
      $io = new SymfonyStyle($input, $output);
      $mail = $input->getArgument('mail');
      $role = $input->getArgument('role');

      if ($mail && $role) {
          if ($user = $this->em->getRepository(User::class)->findOneByEmail($mail)) {
              $this->um->setRole($user, [$role]);
              $io->success("role set");
          } else {
              $io->success('user not found');
          }
      } else {
          $io->success('need to specify a user email and role');
      }

      return Command::SUCCESS;
    }
}
