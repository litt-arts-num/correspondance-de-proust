<?php

namespace App\Command;

use App\Service\LetterManager;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Style\SymfonyStyle;
use Symfony\Component\Filesystem\Filesystem;
use Symfony\Component\Finder\Finder;
use Symfony\Component\HttpFoundation\File\Exception\FileException;

class ImportCommand extends Command
{
    protected static $defaultName = 'app:import';
    protected static $defaultDescription = 'Import letters from "application/import" directory';
    private $lm;

    protected function configure(): void
    {
        $this
            ->addArgument('arg1', InputArgument::OPTIONAL, 'Argument description')
            ->addOption('option1', null, InputOption::VALUE_NONE, 'Option description');
    }

    public function __construct(LetterManager $lm)
    {
        $this->lm = $lm;
        parent::__construct();
    }

    protected function execute(InputInterface $input, OutputInterface $output): int
    {
        $io = new SymfonyStyle($input, $output);
        $finder = new Finder();
        $finder->files()->in(__DIR__ . "/../../import");
        $filesystem = new Filesystem();

        foreach ($finder as $file) {
            $absoluteFilePath = $file->getRealPath();
            $originalFilename = pathinfo($absoluteFilePath, PATHINFO_FILENAME);
            $newFilename = $originalFilename . '-' . uniqid() . '.' . $file->getExtension();
            $path = "public/upload/letters/";
            $io->info("Import : " . $originalFilename);

            try {
                $filesystem->copy($absoluteFilePath, $path . $newFilename, true);
            } catch (FileException $e) {
            }
            $this->lm->postUpload($path, $newFilename, $originalFilename, true);
            $io->success("ok" . $originalFilename);
        }

        $io->success('Import ok');

        return Command::SUCCESS;
    }
}
