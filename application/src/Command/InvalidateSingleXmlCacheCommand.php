<?php

namespace App\Command;

use App\Service\XMLManager;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Style\SymfonyStyle;
use App\Entity\Letter;
use Doctrine\ORM\EntityManagerInterface;

class InvalidateSingleXmlCacheCommand extends Command
{
    protected static $defaultName = 'app:cache:letter:invalidate';
    protected static $defaultDescription = 'single letter cache invalidate';
    private $xm;
    private $em;

    protected function configure(): void
    {
      $this
          ->setDescription('single letter cache invalidate')
          ->addArgument('letterId', InputArgument::REQUIRED, 'letterId')
      ;
    }

    public function __construct(XMLManager $xm, EntityManagerInterface $em)
    {
        $this->xm = $xm;
        $this->em = $em;
        parent::__construct();
    }

    protected function execute(InputInterface $input, OutputInterface $output): int
    {

        $io = new SymfonyStyle($input, $output);
        $letterId = $input->getArgument('letterId');
        $letter = $this->em->getRepository(Letter::class)->findOneByProustId($letterId);

        if($letter) {
            $this->xm->letterCacheInvalidate($letter);
            $io->success('letter cache invalidated');

        } else {
            $io->success('letter not found');
        }
        
        return Command::SUCCESS;
    }
}
