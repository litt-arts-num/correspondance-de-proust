<?php

namespace App\Command;

use App\Entity\PersonCacheUIUC;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Style\SymfonyStyle;

class DeleteUIUCCacheCommand extends Command
{
    protected static $defaultName = 'app:delete-uiuc-cache';
    protected static $defaultDescription = 'Delete UIUC cache.';
    private $em;

    public function __construct(EntityManagerInterface $em)
    {
        $this->em = $em;
        parent::__construct();
    }

    protected function execute(InputInterface $input, OutputInterface $output): int
    {
        $io = new SymfonyStyle($input, $output);

        $caches = $this->em->getRepository(PersonCacheUIUC::class)->findAll();
        foreach ($caches as $cache) {
            $io->info("del : " . $cache->getName());
            $this->em->remove($cache);
        }
        $this->em->flush();

        $io->success('cache deleted');

        return Command::SUCCESS;
    }
}
