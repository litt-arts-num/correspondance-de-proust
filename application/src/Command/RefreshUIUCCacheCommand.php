<?php

namespace App\Command;

use App\Entity\PersonCacheUIUC;
use App\Service\PersonCacheManager;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Style\SymfonyStyle;

class RefreshUIUCCacheCommand extends Command
{
    protected static $defaultName = 'app:refresh-uiuc-cache';
    protected static $defaultDescription = 'Refresh UIUC cache.';
    private $em;
    private $personCacheManager;

    public function __construct(EntityManagerInterface $em, PersonCacheManager $personCacheManager)
    {
        $this->em = $em;
        $this->personCacheManager = $personCacheManager;
        parent::__construct();
    }

    protected function execute(InputInterface $input, OutputInterface $output): int
    {
        $io = new SymfonyStyle($input, $output);

        $caches = $this->em->getRepository(PersonCacheUIUC::class)->findAll();
        foreach ($caches as $cache) {
            $name = $cache->getName();
            $io->info("refreshing : " . $name);
            $this->personCacheManager->update($cache);
        }
        $this->em->flush();

        $io->success('cache refreshed');

        return Command::SUCCESS;
    }
}
